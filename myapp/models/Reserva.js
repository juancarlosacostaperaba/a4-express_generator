// Constructor para el modelo con atributos
let mongoose = require("mongoose");
let Schema = mongoose.Schema;

let reservaSchema = new Schema({
  desde: Date,
  hasta: Date,
  bicicleta: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "Bicicleta"
  },
  usuario: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "Usuario"
  }
});

//Show Reserva
reservaSchema.statics.allReserva = function(cb) {
  return this.find({}, cb);
};

//Create
reservaSchema.statics.add = function(aReserva, cb) {
  return this.create(aReserva, cb);
};

//Find By Id
reservaSchema.statics.findById = function(reservaId, cb) {
  return this.findOne(
    {
      _id: reservaId
    },
    cb
  );
};

//Delete By Id
reservaSchema.statics.removeById = function(reservaId, cb) {
  return this.deleteOne(
    {
      _id: reservaId
    },
    cb
  );
};

//Update By Id
reservaSchema.statics.findAndUpdate = function(reservaId, data, cb) {
  return this.findByIdAndUpdate(
    {
      _id: reservaId
    },
    data,
    cb
  );
};

//Cuántos días está reservada la bicicleta
reservaSchema.methods.diasDeReserva = function() {
  return moment(this.hasta.diff(moment(this.desde), "days") + 1);
};

module.exports = mongoose.model("Reserva", reservaSchema);
