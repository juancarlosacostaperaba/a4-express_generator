// Constructor para el modelo con atributos
let mongoose = require("mongoose");
let Schema = mongoose.Schema;

let bicicletaSchema = new Schema({
  bicicletaID: Number,
  color: String,
  modelo: String,
  ubicacion: {
    type: [Number],
    index: true
  }
});

//Show Bicis
bicicletaSchema.statics.allBicis = function(cb) {
  return this.find({}, cb);
};

//Create
bicicletaSchema.statics.add = function(aBici, cb) {
  return this.create(aBici, cb);
};

//Find By Id
bicicletaSchema.statics.findById = function(anId, cb) {
  return this.findOne(
    {
      _id: anId
    },
    cb
  );
};

//Delete By Id
bicicletaSchema.statics.removeById = function(anId, cb) {
  return this.deleteOne(
    {
      _id: anId
    },
    cb
  );
};

//Update By Id
bicicletaSchema.statics.findAndUpdate = function(anId, data, cb) {
  return this.findByIdAndUpdate(
    {
      _id: anId
    },
    data,
    cb
  );
};

module.exports = mongoose.model("Bicicleta", bicicletaSchema);
