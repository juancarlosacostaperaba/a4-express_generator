let Bicicleta = require("../../models/Bicicleta");

beforeEach(() => {
  Bicicleta.allBicis=[];
});

describe("Bicicleta.allBicis", () => {
  it('Empiza sin elementos', () => {
    expect(Bicicleta.allBicis.length).toBe(0);
  });
});

describe("Bicicleta.add", () => {
    it("Añadimos un elemento", () => {
        expect(Bicicleta.allBicis.length).toBe(0);
        let a = new Bicicleta(1, "Rojo", "Trek", [28.503789, -13.853296]);
        Bicicleta.add(a);
        expect(Bicicleta.allBicis.length).toBe(1);
        expect(Bicicleta.allBicis[0]).toBe(a);
    });
});

describe("Bicicleta.finById", () => {
  it("Debe devolver la bici con id 1", () => {
    expect(Bicicleta.allBicis.length).toBe(0);
    let a = new Bicicleta(1, "Azul", "Urbana", [28.503789, -13.853296]);
    Bicicleta.add(a);
    let targetBici = Bicicleta.findById(1);
    expect(targetBici.id).toBe(1);
    expect(targetBici.color).toBe(a.color);
    expect(targetBici.modelo).toBe(a.modelo);
  });
});

describe("Bicicleta.removeById", () => {
  it('Borra lo que se encuentra dentro del array', () => {
    expect(Bicicleta.allBicis.length).toBe(0);
    let a = new Bicicleta(1, "Azul", "Urbana", [28.503789, -13.853296]);
    Bicicleta.add(a);
    expect(Bicicleta.allBicis.length).toBe(1);
    let targetBici = Bicicleta.removeById(1);
    expect(Bicicleta.allBicis.length).toBe(0);
  });
});
