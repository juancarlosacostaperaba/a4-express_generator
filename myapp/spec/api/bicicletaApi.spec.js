let Bicicleta = require("../../models/Bicicleta");
let request = require("request");
let server = require("../../bin/www");

beforeEach(() => {
  Bicicleta.allBicis=[];
});

describe("Test de la API", () => {
  describe("GET /", () => {
    it("Devuelve status 200", () => {
      expect(Bicicleta.allBicis.length).toBe(0);
      let a = new Bicicleta(1, "Rojo", "Trek", [28.503789, -13.853296]);
      Bicicleta.add(a);
      request.get("http://localhost:3000/api/bicicletas", (error, response, body) => {
        expect(response.statusCode).toBe(200);
      });
    });
  })
});

describe("POST /create", () => {
  it("Devuelve status 201", (done) => {
    let headers = {
      "Content-type": "application/JSON"
    };
    let aBici = '{"id": 10, "color": "Rojo", "modelo": "Trek", "lat": "28.503789", "long": "-13.853296"}';
    request.post({
      headers: headers,
      url: "http://localhost:3000/api/bicicletas/create",
      body: aBici
    }, (error, response, body) => {
      expect(response.statusCode).toBe(201);
      expect(Bicicleta.findById(10).color).toBe("Rojo");
      done();
    });
  });
});

describe("PUT /update", () => {
  it("Devuelve status 201", (done) => {
    let headers = {
      "Content-type": "application/JSON"
    };
    let aBici = '{"id": 10, "color": "Rojo", "modelo": "Trek", "lat": "28.503789", "long": "-13.853296"}';
    let biciNueva = new Bicicleta(15, "Morado", "Nike", [28.503789,-13.853296]);
    Bicicleta.add(biciNueva);
    request.put({
      headers: headers,
      url: "http://localhost:3000/api/bicicletas/15/update",
      body: aBici
    }, (error, response, body) => {
      expect(response.statusCode).toBe(201);
      expect(Bicicleta.findById(10).color).toBe("Rojo");
      done();
    });
  });
});

describe("DELETE /delete", () => {
  it("Devuelve status 204", (done) => {
    let headers = {
      "Content-type": "application/JSON"
    };
    let biciNueva = new Bicicleta(10, "Morado", "Puma", [28.503789,-13.853296]);
    Bicicleta.add(biciNueva);
    request.delete({
      headers: headers,
      url: "http://localhost:3000/api/bicicletas/delete",
      body: '{"id" : 10}'
    }, (error, response, body) => {
      expect(response.statusCode).toBe(204);
      expect(Bicicleta.allBicis.length).toBe(0);
      done();
    });
  });
});
