//=============== IMPORTS ===============//
//Errores
var createError = require("http-errors");
//Express
var express = require("express");
//Favicon
var favicon = require("serve-favicon");
//Path
var path = require("path");
//Cookies
var cookieParser = require("cookie-parser");
//Logger
var logger = require("morgan");
//Mongoose
var mongoose = require("mongoose");
//App
var app = express();
//Login CRUD
var loginRoutes = require("./routes/login");
//Routes
var indexRouter = require("./routes/index");
var usersRouter = require("./routes/users");
//bicicletas CRUD
var bicicletasRouter = require("./routes/bicicletas");
//Usuarios CRUD
var usuariosRouter = require("./routes/usuarios");
//Token CRUD
var tokenRouter = require("./routes/token");
//bicicletas API
var bicicletasAPIRouter = require("./routes/api/bicicletas");
//Usuarios API
var usuariosAPIRouter = require("./routes/api/usuarios");
//Reserva API
var reservasAPIRouter = require("./routes/api/reservas");
//Clase Usuario
var Usuario = require("./models/Usuario");
//Clase Token
var Token = require("./models/Token");
//Passport
const passport = require("./config/passport");
//Express Session
const session = require("express-session");
//Memory Store
const store = new session.MemoryStore();

//=============== DATABASE ===============//
mongoose.connect("mongodb://localhost/red_bicicletas", {
  useUnifiedTopology: true,
  useNewUrlParser: true
});
mongoose.Promise = global.Promise;
let db = mongoose.connection;
//Bind connection to error event (to get notification of connection errors)
db.on("error", console.error.bind("Error de conexión con MongoDB"));

//=============== VIEW ENGINE SETUP ===============//
//Configuración
app.set("views", path.join(__dirname, "views"));
app.set("view engine", "pug");
//Favicon
app.use(favicon(path.join(__dirname, "/public/favicon.png")));
app.use(logger("dev"));
app.use(express.json());
app.use(
  express.urlencoded({
    extended: false
  })
);

//Sesiones
app.use(
  session({
    cookie: { magAge: 240 * 60 * 60 * 1000 }, //Tiempo en milisegundos
    store: store,
    saveUninitialized: true,
    resave: "true",
    secret: "cualquier cosa no pasa nada 477447"
  })
);

//Cookies
app.use(cookieParser());
app.use(passport.initialize());
app.use(passport.session());
app.use(express.static(path.join(__dirname, "public")));

app.get("/login", function(req, res) {
  res.render("session/login");
});

app.post("/login", function(req, res, next) {
  passport.authenticate("local", function(err, usuario, info) {
    if (err) {
      return next(err);
    }

    if (!usuario) {
      return res.render("session/login", { info });
    }

    req.logIn(usuario, function(err) {
      if (err) {
        return next(err);
      }

      return res.redirect("/");
    });
  })(req, res, next);
});

app.get("/logout", function(req, res) {
  req.logOut(); //Limpiamos la sesión
  res.redirect("/login");
});

app.get("/forgotPassword", function(req, res) {
  res.render("session/forgotPassword");
});

app.post("/forgotPassword", function(req, res) {
  Usuario.findOne({ email: req.body.email }, function(err, usuario) {
    if (!usuario)
      return res.render("session/forgotPassword", {
        info: { message: "No existe ese email en nuestra BBDD." }
      });

    usuario.resetPassword(function(err) {
      if (err) return next(err);

      console.log("session/forgotPasswordMessage");
    });

    res.render("session/forgotPasswordMessage");
  });
});

app.get("/resetPassword/:token", function(req, res, next) {
  Token.findOne({ token: req.params.token }, function(err, token) {
    if (!token)
      return res.status(400).send({
        type: "not-verified",
        msg:
          "No existe un usuario asociado al token. Verifique que su token no haya expirado."
      });

    Usuario.findById(token._userId, function(err, usuario) {
      if (!usuario)
        return res
          .status(400)
          .send({ msg: "No existe un usuario asociado al token." });

      res.render("session/resetPassword", { errors: {}, usuario: usuario });
    });
  });
});

app.post("/resetPassword", function(req, res) {
  if (req.body.password != req.body.confirm_password) {
    res.render("session/resetPassword", {
      errors: {
        confirm_password: {
          message: "No coincide con el password introducido."
        }
      },

      usuario: new Usuario({ email: req.body.email })
    });

    return;
  }

  Usuario.findOne({ email: req.body.email }, function(err, usuario) {
    (usuario.password = req.body.password),
      usuario.save(function(err) {
        if (err) {
          res.render("session/resetPassword", {
            errors: err.errors,
            usuario: new Usuario({ email: req.body.email })
          });
        } else {
          res.redirect("/login");
        }
      });
  });
});

//=============== CRUD ===============//
//Login CRUD
app.use("/login", loginRoutes);
//bicicletas CRUD
app.use('/bicicletas', loggedIn, bicicletasRouter);
//Token CRUD
app.use("/token", tokenRouter);
//Usuarios CRUD
app.use("/usuarios", loggedIn, usuariosRouter);

app.use("/", indexRouter);
//app.use('/users', usersRouter);

//=============== API ===============//
//bicicletas API
app.use("/api/bicicletas", bicicletasAPIRouter);
//Usuarios API
app.use("/api/usuarios", usuariosAPIRouter);
//Reserva API
app.use("/api/reservas", reservasAPIRouter);

//=============== ERRORES ===============//
// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

//error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get("env") === "development" ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render("error");
});

//=============== MIDDLEWARE ===============//
function loggedIn(req, res, next) {
  if (req.user) {
    next();
  } else {
    console.log("Usuario no logueado");

    res.redirect("/login");
  }
}

module.exports = app;
