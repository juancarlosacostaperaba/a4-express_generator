let Usuario = require("../../models/Usuario");

//Show Usuarios
exports.usuarios_list = function(req, res) {
  Usuario.allUsuarios(function(err, usuarios) {
    if (err) {
      res.status(500).send(err.message);
    }
    res.status(200).send(usuarios);
  });
};

//Create
exports.usuario_create = function(req, res) {
  Usuario.add(
    {
      nombre: req.body.nombre
    },
    function(err, usuario) {
      if (err) {
        res.status(500).send(err.message);
      }
      res.status(201).send(usuario);
    }
  );
};

//Delete By Id
exports.usuario_delete = function(req, res) {
  Usuario.removeById(req.body._id, function(err, usuario) {
    if (err) {
      res.status(500).send(err.message);
    }
    res.status(204).send(usuario);
  });
};

//Update By Id
exports.usuario_update = function(req, res) {
  Usuario.findAndUpdate(
    req.params._id,
    {
      nombre: req.body.nombre
    },
    function(err, usuario) {
      if (err) {
        res.status(500).send(err.message);
      }
      res.status(201).send(usuario);
    }
  );
};

//Reservar
exports.usuarios_reservar = function(req, res) {
  Usuario.findById(req.body._id, function(err, usuario) {
    if (err) {
      res.status(500).send(err.message);
    }
    console.log(usuario);
    usuario.reservar(req.body.bicicleta, req.body.desde, req.body.hasta, function(
      err
    ) {
      console.log("Reservada!!");
      res.status(200).send(usuario);
    });
  });
};
