let Reserva = require("../../models/Reserva");

//Show Reservas
exports.reserva_list = function(req, res) {
  Reserva.allReserva(function(err, reservas) {
    if (err) {
      res.status(500).send(err.message);
    }
    res.status(200).send(reservas);
  });
};

//Create
exports.reserva_create = function(req, res) {
  Reserva.add(
    {
      desde: req.body.desde,
      hasta: req.body.hasta,
      bicicleta: req.body.bicicleta,
      usuario: req.body.usuario
    },
    function(err, reserva) {
      if (err) {
        res.status(500).send(err.message);
      }
      res.status(201).send(reserva);
    }
  );
};

//Delete By Id
exports.reserva_delete = function(req, res) {
  Reserva.removeById(req.body._id, function(err, reserva) {
    if (err) {
      res.status(500).send(err.message);
    }
    res.status(204).send(reserva);
  });
};

//Update By Id
exports.reserva_update = function(req, res) {
  Reserva.findAndUpdate(
    req.params._id,
    {
      desde: req.body.desde,
      hasta: req.body.hasta,
      bicicleta: req.body.bicicleta,
      usuario: req.body.usuario
    },
    function(err, reserva) {
      if (err) {
        res.status(500).send(err.message);
      }
      res.status(201).send(reserva);
    }
  );
};
