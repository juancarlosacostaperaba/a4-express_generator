let Bicicleta = require("../../models/Bicicleta");

//Show Bicis
exports.bicicleta_list = function(req, res) {
  Bicicleta.allBicis(function(err, bicis) {
    if (err) {
      res.status(500).send(err.message);
    }
    res.status(200).send(bicis);
  });
};

//Create
exports.bicicleta_create = function(req, res) {
  Bicicleta.add(
    {
      bicicletaID: req.body.bicicletaID,
      color: req.body.color,
      modelo: req.body.modelo,
      ubicacion: [req.body.latitud, req.body.longitud]
    },
    function(err, bici) {
      if (err) {
        res.status(500).send(err.message);
      }
      res.status(201).send(bici);
    }
  );
};

//Delete By Id
exports.bicicleta_delete = function(req, res) {
  Bicicleta.removeById(req.body._id, function(err, bici) {
    if (err) {
      res.status(500).send(err.message);
    }
    res.status(204).send(bici);
  });
};

//Update By Id
exports.bicicleta_update = function(req, res) {
  Bicicleta.findAndUpdate(
    req.params._id,
    {
      bicicletaID: req.body.bicicletaID,
      color: req.body.color,
      modelo: req.body.modelo,
      ubicacion: [req.body.latitud, req.body.longitud]
    },
    function(err, bici) {
      if (err) {
        res.status(500).send(err.message);
      }
      res.status(201).send(bici);
    }
  );
};
