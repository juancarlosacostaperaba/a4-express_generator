let Bicicleta = require("../models/Bicicleta.js");

//////////////////////////////////////
// Lista (array) de las bicicletas //
////////////////////////////////////
exports.bicicleta_list = function(req, res) {
  Bicicleta.allBicis(function(err, bicis) {
    if (err) {
      res.status(500).send(err.message);
    }
    res.render("bicicletas/index", {
      bicis: bicis
    });
  });
};

////////////////////////////////////
// Creación de nuevas bicicletas //
//////////////////////////////////
exports.bicicleta_create_get = function(req, res) {
  res.render("bicicletas/create");
};

exports.bicicleta_create_post = function(req, res) {
  Bicicleta.add(
    {
      bicicletaID: req.body.bicicletaID,
      color: req.body.color,
      modelo: req.body.modelo,
      ubicacion: [req.body.latitud, req.body.longitud]
    },
    function(err, bicis) {
      if (err) {
        res.status(500).send(err.message);
      }
      res.redirect("/bicicletas");
    }
  );
};

////////////////////////////
// Borrado de bicicletas //
//////////////////////////
exports.bicicleta_delete_post = function(req, res) {
  Bicicleta.removeById(req.body._id, function(err, bicis) {
    if (err) {
      res.status(500).send(err.message);
    }
    res.redirect("/bicicletas");
  });
};

///////////////////////////////////////////////
// Actualización de datos de las bicicletas //
/////////////////////////////////////////////
exports.bicicleta_update_get = function(req, res) {
  Bicicleta.findById(
    {
      _id: req.params._id
    },
    function(err, bici) {
      if (err) {
        res.status(500).send(err.message);
      }
      console.log(bici.bicicletaID);
      res.render("bicicletas/update", {
        bici: bici
      });
    }
  );
};

exports.bicicleta_update_post = function(req, res) {
  Bicicleta.findAndUpdate(
    req.params._id,
    {
      bicicletaID: req.body.bicicletaID,
      color: req.body.color,
      modelo: req.body.modelo,
      ubicacion: [req.body.latitud, req.body.longitud]
    },
    function(err, bicis) {
      if (err) {
        res.status(500).send(err.message);
      }
      res.redirect("/bicicletas");
    }
  );
};
