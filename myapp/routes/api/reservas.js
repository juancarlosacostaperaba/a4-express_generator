let express = require("express");
let router = express.Router();
let reservaControllerAPI = require("../../controllers/api/reservaControllerAPI");

//Show Reservas
router.get("/", reservaControllerAPI.reserva_list);

//Create
router.post("/create", reservaControllerAPI.reserva_create);

//Delete By Id
router.delete("/delete", reservaControllerAPI.reserva_delete);

//Update By Id
router.put("/:_id/update", reservaControllerAPI.reserva_update);

module.exports = router;
