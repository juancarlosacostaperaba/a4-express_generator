let express = require("express");
let router = express.Router();
let usuariosControllerAPI = require("../../controllers/api/usuariosControllerAPI");

//Show Usuarios
router.get("/", usuariosControllerAPI.usuarios_list);

//Create
router.post("/create", usuariosControllerAPI.usuario_create);

//Delete By Id
router.delete("/delete", usuariosControllerAPI.usuario_delete);

//Update By Id
router.put("/:_id/update", usuariosControllerAPI.usuario_update);

//Reserva
router.post("/reservar", usuariosControllerAPI.usuarios_reservar);

module.exports = router;
