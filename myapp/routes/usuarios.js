let express = require("express");
let router = express.Router();
let usuarioController = require("../controllers/usuario");

// Listado de usuarios
router.get("/", usuarioController.list);

// Creación de usuarios
router.get("/create", usuarioController.create_get);
router.post("/create", usuarioController.create);

// Borrado de usuarios
router.post("/delete", usuarioController.delete);

// Actualización de usuarios
router.get("/:_id/update", usuarioController.update_get);
router.post("/:_id/update", usuarioController.update);

module.exports = router;
