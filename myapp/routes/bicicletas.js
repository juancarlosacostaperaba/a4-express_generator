var express = require('express');
var router = express.Router();
var bicicletaController = require('../controllers/bicicleta');

// Lista de bicicletas
router.get("/", bicicletaController.bicicleta_list);

// Creación de bicicletas
router.get("/create", bicicletaController.bicicleta_create_get);
router.post("/create", bicicletaController.bicicleta_create_post);

// Borrado de bicicletas
router.post("/delete", bicicletaController.bicicleta_delete_post);

// Actualización de bicicletas
router.get("/:_id/update", bicicletaController.bicicleta_update_get);
router.post("/:_id/update", bicicletaController.bicicleta_update_post);

module.exports = router;
